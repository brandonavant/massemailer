﻿namespace MassEmailer.Models
{
    public class Recipient
    {
        public string Name { get; set; }
        public string EmailAddress { get; set; }
    }
}

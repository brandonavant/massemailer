﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace MassEmailer.Models
{
    public class ConfigFile
    {
        public string SmtpHost { get; set; }
        public string SmtpPort { get; set; }
        public string LoginAccount { get; set; }
        public string LoginPassword { get; set; }
        public string SenderEmailAddress { get; set; }
        public bool SendCopyToSender { get; set; }
        public string AttachmentPath { get; set; }
    }
}

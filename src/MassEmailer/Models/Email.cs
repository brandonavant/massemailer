﻿using System.Collections.Generic;

namespace MassEmailer.Models
{
    public class Email
    {
        public string Subject { get; set; }
        public string Body { get; set; }
        public List<Recipient> Recipients { get; set; }
    }
}

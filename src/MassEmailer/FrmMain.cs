﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Threading;
using System.Windows.Forms;
using MassEmailer.Models;
using MassEmailer.Properties;
using Newtonsoft.Json;
using OfficeOpenXml;

namespace MassEmailer
{
    public partial class FrmMain : Form
    {
        #region Private Variables

        private readonly OpenFileDialog _ofdExcelFile;
        private readonly Email _email;
        private readonly ConfigFile _configFile;

        private enum WorksheetColumns
        {
            Name = 1,
            EmailAddress
        }

        #endregion

        #region Constructors

        public FrmMain()
        {
            InitializeComponent();

            btnSendEmails.Enabled = false;
            lblError.Visible = false;

            rtfBody.Text = Resources.DefaultBody;
            txtSubject.Text = Resources.DefaultSubject;

            _ofdExcelFile = new OpenFileDialog
            {
                InitialDirectory = System.IO.Directory.GetCurrentDirectory()
            };

            _email = new Email();
            _configFile = GetConfigFile();

            llbAttachment.Text = _configFile.AttachmentPath ?? "None";
        }

        #endregion

        #region Form Event Handlers

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            if (_ofdExcelFile.ShowDialog() != DialogResult.OK) return;

            lblSelectedFile.Text = _ofdExcelFile.FileName;
            btnSendEmails.Enabled = true;

            _email.Recipients = ParseExcelFile(_ofdExcelFile.FileName);
        }

        private void btnSendEmails_Click(object sender, EventArgs e)
        {
            lblError.Visible = false;

            if (txtSubject.Text != string.Empty)
            {
                _email.Subject = txtSubject.Text;
            }
            else
            {
                lblError.Text = Resources.MissingEmailSubject;
                return;
            }

            if (rtfBody.Text != string.Empty)
            {
                _email.Body = rtfBody.Text;
            }
            else
            {
                lblError.Text = Resources.MissingEmailBody;
                return;
            }

            if (_email.Recipients.Count == 0)
            {
                lblError.Text = Resources.MissingEmailRecipients;
                return;
            }

            SendEmails();
        }

        private void smiGenerateConfig_Click(object sender, EventArgs e)
        {
            try
            {
                using (var fw = new StreamWriter(System.IO.Directory.GetCurrentDirectory() + "\\EmailConfig.json"))
                {
                    _configFile.SenderEmailAddress = "put_the_display_email_here";
                    _configFile.LoginAccount = "put_login_account_here";
                    _configFile.LoginPassword = "put_login_password_here";
                    _configFile.SmtpHost = "smtp-mail.outlook.com";
                    _configFile.SmtpPort = "587";
                    _configFile.AttachmentPath = "attachment.pdf";

                    fw.Write(JsonConvert.SerializeObject(_configFile));
                }

                MessageBox.Show(Resources.ConfigFileGenerated);
            }
            catch
            {
                lblError.Text = Resources.ExceptionOccurred;
            }
        }

        private void smiExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void smiAbout_Click(object sender, EventArgs e)
        {
            var frmAbout = new FrmAbout();

            frmAbout.Show();
        }

        private void smiInstructions_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(System.IO.Directory.GetCurrentDirectory() + @"\README.txt");
            }
            catch
            {
                lblError.Text = Resources.ReadmeNotFound;
            }
        }

        #endregion

        #region Private Methods

        private List<Recipient> ParseExcelFile(string filePath)
        {
            var excelPackage = new ExcelPackage(new FileInfo(filePath));
            var excelWorksheet = excelPackage.Workbook.Worksheets.FirstOrDefault();
            var recipients = new List<Recipient>();

            if (excelWorksheet == null) return recipients;

            for (var row = excelWorksheet.Dimension.Start.Row + 1; row <= excelWorksheet.Dimension.End.Row; row++)
            {
                var parsedName = excelWorksheet.Cells[row, (int)WorksheetColumns.Name].Value?.ToString();
                var parsedEmailAddress = excelWorksheet.Cells[row, (int)WorksheetColumns.EmailAddress].Value?.ToString();

                if (parsedName == null || parsedEmailAddress == null) { continue; }

                recipients.Add(new Recipient()
                {
                    Name = excelWorksheet.Cells[row, (int)WorksheetColumns.Name].Value?.ToString(),
                    EmailAddress = excelWorksheet.Cells[row, (int)WorksheetColumns.EmailAddress].Value?.ToString()
                });
            }

            lblNumberOfRecipients.Text = recipients.Count.ToString();

            return recipients;
        }

        private void SendEmails()
        {
            if (!ValidateConfigFile()) return;

            var emailTimer = new System.Timers.Timer(3000); // Only send 20 per minute

            emailTimer.Start();

            for (var iterator = 0; iterator < _email.Recipients.Count; iterator++)
            {
                btnSendEmails.Enabled = false;
                btnSendEmails.Text = Resources.PleaseWait;

                var smtpPort = 587;

                var mailMessage = new MailMessage();
                var smtpClient = new SmtpClient();

                try
                {
                    int.TryParse(_configFile.SmtpPort, out smtpPort);

                    if (_configFile.SendCopyToSender)
                    {
                        mailMessage.Bcc.Add(_configFile.SenderEmailAddress);
                    }

                    mailMessage.To.Add(_email.Recipients[iterator].EmailAddress);
                    mailMessage.From = new MailAddress(_configFile.SenderEmailAddress);
                    mailMessage.Subject = _email.Subject.Replace("{name}", _email.Recipients[iterator].Name);
                    mailMessage.Body = _email.Body.Replace("{name}", _email.Recipients[iterator].Name);
                    mailMessage.IsBodyHtml = false;

                    if (!String.IsNullOrEmpty(_configFile.AttachmentPath))
                    {
                        mailMessage.Attachments.Add(new Attachment(_configFile.AttachmentPath));
                    }

                    smtpClient.Host = _configFile.SmtpHost;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new System.Net.NetworkCredential(_configFile.LoginAccount, _configFile.LoginPassword);
                    smtpClient.Port = smtpPort;
                    smtpClient.EnableSsl = true;

                    smtpClient.Send(mailMessage);
                }
                catch (Exception ex)
                {
                    lblError.Text = Resources.FailedToSendEmails
                        .Replace("{0}", (iterator - 1).ToString())
                        .Replace("{1}", _email.Recipients.Count.ToString());
                }

                Thread.Sleep(3000);
            }

            btnSendEmails.Text = Resources.SendEmails;
            btnSendEmails.Enabled = true;

            MessageBox.Show(Resources.Done);
        }

        private bool ValidateConfigFile()
        {
            return _configFile.SmtpPort != string.Empty
                   && _configFile.SmtpHost != string.Empty
                   && _configFile.LoginAccount != string.Empty
                   && _configFile.LoginPassword != string.Empty
                   && _configFile.SenderEmailAddress != string.Empty;
        }

        private ConfigFile GetConfigFile()
        {
            var configFIlePath = System.IO.Directory.GetCurrentDirectory() + "\\EmailConfig.json";
            var configFile = new ConfigFile();

            try
            {
                using (var streamReader = new StreamReader(configFIlePath))
                {
                    var configBody = streamReader.ReadToEnd();
                    configFile = JsonConvert.DeserializeObject<ConfigFile>(configBody);
                }
            }
            catch
            {
                lblError.Text = Resources.ExceptionOccurred;
            }

            return configFile;
        }

        #endregion

        private void llbAttachment_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (File.Exists(_configFile?.AttachmentPath))
            {
                try
                {
                    System.Diagnostics.Process.Start(_configFile.AttachmentPath);
                }
                catch(FileNotFoundException)
                {
                    lblError.Text = Resources.UnableToOpenAttachment;
                }
            }
        }
    }
}

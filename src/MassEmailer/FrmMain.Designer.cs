﻿namespace MassEmailer
{
    partial class FrmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smiExit = new System.Windows.Forms.ToolStripMenuItem();
            this.smiGenerateConfig = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smiInstructions = new System.Windows.Forms.ToolStripMenuItem();
            this.smiAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.rtfBody = new System.Windows.Forms.RichTextBox();
            this.btnSelectFile = new System.Windows.Forms.Button();
            this.btnSendEmails = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.lblNumberOfRecipients = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.lblSelectedFile = new System.Windows.Forms.TextBox();
            this.llbAttachment = new System.Windows.Forms.LinkLabel();
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.smiGenerateConfig,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(623, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.smiExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // smiExit
            // 
            this.smiExit.Name = "smiExit";
            this.smiExit.Size = new System.Drawing.Size(92, 22);
            this.smiExit.Text = "Exit";
            this.smiExit.Click += new System.EventHandler(this.smiExit_Click);
            // 
            // smiGenerateConfig
            // 
            this.smiGenerateConfig.Name = "smiGenerateConfig";
            this.smiGenerateConfig.Size = new System.Drawing.Size(126, 20);
            this.smiGenerateConfig.Text = "Generate Config File";
            this.smiGenerateConfig.Click += new System.EventHandler(this.smiGenerateConfig_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.smiInstructions,
            this.smiAbout});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // smiInstructions
            // 
            this.smiInstructions.Name = "smiInstructions";
            this.smiInstructions.Size = new System.Drawing.Size(136, 22);
            this.smiInstructions.Text = "Instructions";
            this.smiInstructions.Click += new System.EventHandler(this.smiInstructions_Click);
            // 
            // smiAbout
            // 
            this.smiAbout.Name = "smiAbout";
            this.smiAbout.Size = new System.Drawing.Size(136, 22);
            this.smiAbout.Text = "About";
            this.smiAbout.Click += new System.EventHandler(this.smiAbout_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 36);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Subject:";
            // 
            // txtSubject
            // 
            this.txtSubject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSubject.Location = new System.Drawing.Point(56, 33);
            this.txtSubject.Margin = new System.Windows.Forms.Padding(2);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(560, 20);
            this.txtSubject.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 71);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Body:";
            // 
            // rtfBody
            // 
            this.rtfBody.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtfBody.Location = new System.Drawing.Point(9, 87);
            this.rtfBody.Margin = new System.Windows.Forms.Padding(2);
            this.rtfBody.Name = "rtfBody";
            this.rtfBody.Size = new System.Drawing.Size(606, 187);
            this.rtfBody.TabIndex = 4;
            this.rtfBody.Text = "";
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectFile.Location = new System.Drawing.Point(9, 278);
            this.btnSelectFile.Margin = new System.Windows.Forms.Padding(2);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(104, 26);
            this.btnSelectFile.TabIndex = 5;
            this.btnSelectFile.Text = "Choose Excel File";
            this.btnSelectFile.UseVisualStyleBackColor = true;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // btnSendEmails
            // 
            this.btnSendEmails.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendEmails.Location = new System.Drawing.Point(511, 382);
            this.btnSendEmails.Margin = new System.Windows.Forms.Padding(2);
            this.btnSendEmails.Name = "btnSendEmails";
            this.btnSendEmails.Size = new System.Drawing.Size(104, 26);
            this.btnSendEmails.TabIndex = 6;
            this.btnSendEmails.Text = "Send Emails";
            this.btnSendEmails.UseVisualStyleBackColor = true;
            this.btnSendEmails.Click += new System.EventHandler(this.btnSendEmails_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 327);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(145, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Number of Recipients Found:";
            // 
            // lblNumberOfRecipients
            // 
            this.lblNumberOfRecipients.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblNumberOfRecipients.AutoSize = true;
            this.lblNumberOfRecipients.Location = new System.Drawing.Point(151, 327);
            this.lblNumberOfRecipients.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblNumberOfRecipients.Name = "lblNumberOfRecipients";
            this.lblNumberOfRecipients.Size = new System.Drawing.Size(13, 13);
            this.lblNumberOfRecipients.TabIndex = 10;
            this.lblNumberOfRecipients.Text = "0";
            // 
            // lblError
            // 
            this.lblError.AutoSize = true;
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(7, 366);
            this.lblError.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(152, 13);
            this.lblError.TabIndex = 11;
            this.lblError.Text = "Error: This is an error message.";
            // 
            // lblSelectedFile
            // 
            this.lblSelectedFile.BackColor = System.Drawing.SystemColors.Control;
            this.lblSelectedFile.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblSelectedFile.Location = new System.Drawing.Point(118, 285);
            this.lblSelectedFile.Margin = new System.Windows.Forms.Padding(2);
            this.lblSelectedFile.Multiline = true;
            this.lblSelectedFile.Name = "lblSelectedFile";
            this.lblSelectedFile.ReadOnly = true;
            this.lblSelectedFile.Size = new System.Drawing.Size(496, 39);
            this.lblSelectedFile.TabIndex = 12;
            // 
            // llbAttachment
            // 
            this.llbAttachment.AutoSize = true;
            this.llbAttachment.Location = new System.Drawing.Point(73, 395);
            this.llbAttachment.Name = "llbAttachment";
            this.llbAttachment.Size = new System.Drawing.Size(33, 13);
            this.llbAttachment.TabIndex = 13;
            this.llbAttachment.TabStop = true;
            this.llbAttachment.Text = "None";
            this.llbAttachment.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llbAttachment_LinkClicked);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 395);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Attachment:";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 418);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.llbAttachment);
            this.Controls.Add(this.lblSelectedFile);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.lblNumberOfRecipients);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSendEmails);
            this.Controls.Add(this.btnSelectFile);
            this.Controls.Add(this.rtfBody);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSubject);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "FrmMain";
            this.Text = "Mass Emailer";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smiExit;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smiInstructions;
        private System.Windows.Forms.ToolStripMenuItem smiAbout;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox rtfBody;
        private System.Windows.Forms.Button btnSelectFile;
        private System.Windows.Forms.Button btnSendEmails;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblNumberOfRecipients;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.TextBox lblSelectedFile;
        private System.Windows.Forms.ToolStripMenuItem smiGenerateConfig;
        private System.Windows.Forms.LinkLabel llbAttachment;
        private System.Windows.Forms.Label label3;
    }
}

